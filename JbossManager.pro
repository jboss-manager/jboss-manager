# -------------------------------------------------
# Project created by QtCreator 2009-09-03T15:41:14
# -------------------------------------------------
QT += xml network
TARGET = JbossManager
TEMPLATE = app
SOURCES += main.cpp \
    jbossmanager.cpp \
    settingsdialog.cpp
HEADERS += jbossmanager.h \
    settingsdialog.h
FORMS += jbossmanager.ui \
    settingsdialog.ui
RESOURCES += img.qrc




