//    JbossManager copyright 2009 Albertini Riccardo
//
//    This file is part of JbossManager.
//
//    JbossManager is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    JbossManager is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with JbossManager.  If not, see <http://www.gnu.org/licenses/>.

#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "jbossmanager.h"

SettingsDialog::SettingsDialog(QWidget *parent, bool protect) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    setting = new QSettings("sirowain.com", "JBoss Manager");
    ui->lineEditJB->setText(setting->value("jboss-home", getenv("JBOSS_HOME")).toString());
    ui->consoleSizeSpin->setValue(setting->value("console-size", 1000).toInt());
    QFont *font = new QFont();
    font->fromString(setting->value("font-name", "Courier").toString());
    ui->fontComboBox->setCurrentFont(*font);
    ui->spinBoxFontSize->setValue(setting->value("font-size", 9).toInt());
    font->~QFont();
    if (protect)
    {
        ui->lineEditJB->setEnabled(false);
        ui->buttonBrowseJB->setEnabled(false);
        ui->buttonDefaultJB->setEnabled(false);
    }
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::accept()
{
    QFileInfo file( ui->lineEditJB->text().toUtf8().replace("\\", "/").append("/bin/run.jar") );
    if( !file.exists() )
    {
        QMessageBox msgBox(this);
        msgBox.setText("The path is not a valid JBoss root folder:");
        msgBox.setInformativeText("There's no a \\bin\\run.jar file in there.");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    }
    else{
        setting->setValue("jboss-home", ui->lineEditJB->text().toUtf8());
        setting->setValue("console-size", ui->consoleSizeSpin->value());
        setting->setValue("font-name", ui->fontComboBox->currentFont().toString());
        setting->setValue("font-size", ui->spinBoxFontSize->value());
        this->done(1);
    }

}

void SettingsDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void SettingsDialog::selectJBoss()
{
    QString filename = QFileDialog::getExistingDirectory(this,
    tr("Select directory"), ".");
    if (filename.trimmed()!="")
        ui->lineEditJB->setText(filename);
}

QString SettingsDialog::getJBRoot()
{
    return ui->lineEditJB->text();
}

void SettingsDialog::setDefault()
{
    ui->lineEditJB->setText(getenv("JBOSS_HOME"));
}

int SettingsDialog::getConsoleSize()
{
    return ui->consoleSizeSpin->value();
}

QString SettingsDialog::getFontName()
{
    return ui->fontComboBox->currentFont().toString();
}

int SettingsDialog::getFontSize()
{
    return ui->spinBoxFontSize->value();
}
