#ifndef JBOSSMANAGER_H
#define JBOSSMANAGER_H

#include <QtGui/QMainWindow>
#include <QProcess>
#include <QDirModel>
#include <QListWidget>
#include <QDir>
#include <QSystemTrayIcon>
#include <QPushButton>
#include <QMessageBox>
#include <QLineEdit>
#include <QFont>
#include <QSettings>
#include <QTextCursor>
#include <QNetworkProxy>
#include <QNetworkProxyQuery>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QAbstractNetworkCache>
#include <QFile>

#if defined (__WIN32__)
    #include <windows.h>
#endif


namespace Ui
{
    class JbossManager;
}

class JbossManager : public QMainWindow
{
    Q_OBJECT

public:
    JbossManager(QWidget *parent = 0);
    ~JbossManager();
    void closeEvent(QCloseEvent*);
    void updateGUI(QString);
    QSettings *setting;

private:
    Ui::JbossManager *ui;
    QString APP_NAME;
    QString APP_VER;
    QProcess* proc;
    QDirModel *model;
    QPushButton *clear_bt;
    QSystemTrayIcon *trayIcon;
    QString *trayAlreadyExists;    
    QString *jboss;
    QNetworkAccessManager manager;
    QNetworkReply *reply;
    void executeJboss(QString);
    void executeJbossNew(QString);
    void getProxy();
    void checkUpdate();

private slots:
    void run();
    void stop();
    void output();
    void endMessage();
    void setTitle(QString);
    void clear();
    bool quit();
    void search_forward();
    void search_backward();
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void about();
    void about_qt();
    void openJBFolder();
    void settings();
    void load();
    void escEvent();
    void readUpdate();

signals:
    void started();
};

#endif // JBOSSMANAGER_H
