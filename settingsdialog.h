#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QString>
#include <QFileDialog>
#include <QSettings>
#include <QMessageBox>
#include <QProcess>
#include <QBool>

namespace Ui {
    class SettingsDialog;
}

class SettingsDialog : public QDialog {
    Q_OBJECT
public:
    SettingsDialog(QWidget *parent = 0, bool protect= false);
    ~SettingsDialog();
    QString getJBRoot();
    int getConsoleSize();
    QString getFontName();
    int getFontSize();

protected:
    void changeEvent(QEvent *e);
    void accept();

private:
    Ui::SettingsDialog *ui;
    QSettings *setting;

private slots:
    void selectJBoss();
    void setDefault();
};

#endif // SETTINGSDIALOG_H
