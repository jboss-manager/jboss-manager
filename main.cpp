//    JbossManager copyright 2009 Albertini Riccardo
//
//    This file is part of JbossManager.
//
//    JbossManager is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    JbossManager is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with JbossManager.  If not, see <http://www.gnu.org/licenses/>.

#include <QtGui/QApplication>
#include "jbossmanager.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    JbossManager w;
    w.show();
    return a.exec();
}
