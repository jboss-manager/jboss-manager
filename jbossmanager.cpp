//    JbossManager copyright 2009 Albertini Riccardo
//
//    This file is part of JbossManager.
//
//    JbossManager is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    JbossManager is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with JbossManager.  If not, see <http://www.gnu.org/licenses/>.

#include "jbossmanager.h"
#include "ui_jbossmanager.h"
#include "settingsdialog.h"
#include <QTextBlock>
#include <QDesktopServices>
#include <QUrl>

JbossManager::JbossManager(QWidget *parent)
        : QMainWindow(parent), ui(new Ui::JbossManager)
{
    #if defined(_WIN32) || defined(WIN32) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__BORLANDC__)
    #define OS_WIN
    #endif
    ui->setupUi(this);
    APP_NAME = "JBoss Manager";
    APP_VER = "0.5";
    setting = new QSettings("sirowain.com", "JBoss Manager");
    restoreGeometry(setting->value("geometry").toByteArray());
    ui->plainTextEdit->setMaximumBlockCount(setting->value("console-size", 1000).toInt());
    proc = new QProcess();
    connect(proc, SIGNAL(readyRead()), this, SLOT(output()));
    connect(proc, SIGNAL(finished(int)), this, SLOT(endMessage()));
    connect(ui->comboBox, SIGNAL(currentIndexChanged(QString)),this,SLOT(setTitle(QString)));
    load();
    getProxy();
    checkUpdate();

    // Output Area Context Menu
    ui->plainTextEdit->addAction(ui->actionCopy);
    ui->plainTextEdit->addAction(ui->actionSelect_All);
    QAction *sep = new QAction(this);
    sep->setSeparator(true);
    ui->plainTextEdit->addAction(sep);
    ui->plainTextEdit->addAction(ui->actionClear);

    // Search tool
    ui->searchWidget->addAction(ui->actionEsc);
    ui->searchWidget->setVisible(false);

    ui->pushButton->setEnabled(false);
    trayAlreadyExists = new QString("not exists");
    updateGUI(QString("1"));
}

JbossManager::~JbossManager()
{
    delete ui;
    proc->kill();
}

void JbossManager::closeEvent(QCloseEvent *event)
{
    if (event->spontaneous())
    {
        iconActivated(QSystemTrayIcon::DoubleClick);
        event->ignore();
    }
    else
    {
        if (quit()) {
            setting->setValue("geometry", saveGeometry());
            setting->setValue("last-server", ui->comboBox->currentText());
            event->accept();
        } else {
            event->ignore();
        }
    }
}

void JbossManager::load()
{
    // Settings
    ui->plainTextEdit->setMaximumBlockCount(setting->value("console-size", 1000).toInt());
    // Set Font
    QFont *font = new QFont();
    font->fromString(setting->value("font-name", "Courier").toString());
    font->setPointSize(setting->value("font-size", 9).toInt());
    ui->plainTextEdit->setFont(*font);
    font->~QFont();
    // Combo JBoss
    ui->comboBox->clear();
    jboss = new QString(setting->value("jboss-home").toString());
        if (*jboss=="")
            jboss = new QString(getenv("JBOSS_HOME"));
    QDir *dir = new QDir(*jboss + "/server");
    QStringList list = dir->entryList(QDir::AllDirs | QDir::NoDotAndDotDot,QDir::Name);
    ui->comboBox->insertItems(0,list);
    int selected;
    selected = ui->comboBox->findText(setting->value("last-server").toString(), Qt::MatchFixedString);
    if (selected<0)
        ui->comboBox->setCurrentIndex(0);
    else
        ui->comboBox->setCurrentIndex(selected);
}

void JbossManager::updateGUI(QString showTray)
{
    // Tray Icon
    if (showTray == "1" && trayAlreadyExists->toStdString()=="not exists")
    {
        trayIcon = new QSystemTrayIcon(QIcon(":/img/jboss.png"),this);
        trayIcon->setToolTip(APP_NAME);
        trayIcon->show();
        connect(trayIcon,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),this,
                SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
        trayIcon->setContextMenu(ui->menu_File);
        ui->actionStop->setEnabled(false);
        trayAlreadyExists = new QString("exists");
    }
    else if(showTray == "0" && trayAlreadyExists->toStdString()=="exists")
    {
        trayIcon->hide();
        trayAlreadyExists = new QString("not exists");
    }
}

void JbossManager::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
        if (this->isVisible())
            this->hide();
        else
        {
            this->raise();
            this->setFocus();
            this->show();
            #if defined (__WIN32__)
            if(IsIconic(this->winId()))
                ShowWindow(this->winId(),SW_RESTORE);
            else
                SetForegroundWindow( this->winId() );
            #endif
            }

    case QSystemTrayIcon::MiddleClick:
    default:
        ;
    }
}

void JbossManager::executeJboss(QString hub)
{
    QString java(getenv("JAVA_HOME"));
    QString program = "\"" + java + "/bin/java\" -jar " + "\"" + *jboss + "/bin/run.jar\" -c " + hub;
    proc->start(program);
}

void JbossManager::executeJbossNew(QString hub)
{
    QString program = "\"" + *jboss + "/bin/run.sh\" -c " + hub;
    proc->start(program);
}

void JbossManager::run()
{
    QString hub(ui->comboBox->currentText());
    ui->plainTextEdit->clear();
    /* start process */
    executeJboss(hub);
    /* update ui */
    this->setWindowTitle(APP_NAME + " - " + hub + " (Running)");
    ui->statusBar->showMessage("Starting Server...");
    ui->comboBox->setEnabled(false);
    ui->pushButton_2->setEnabled(false);
    ui->pushButton->setEnabled(true);
    trayIcon->setToolTip(APP_NAME + " - " + hub);
    trayIcon->setIcon(QIcon(":/img/jboss_starting.png"));
    this->setWindowIcon(QIcon(":/img/jboss_starting.png"));
    ui->actionStart->setEnabled(false);
    ui->actionStop->setEnabled(true);
    ui->action_Reload->setEnabled(false);
}

void JbossManager::stop()
{
    ui->pushButton_2->setText("Stopping...");
    #ifdef OS_WIN
        proc->kill();
    #else
        proc->terminate();
    #endif
}

void JbossManager::output()
{
    QString output(proc->readAll());
    if (output.contains("JBoss") && output.contains("Started in"))
    {
        ui->statusBar->showMessage("Server running");
        if (this->isHidden())
            trayIcon->showMessage(ui->comboBox->currentText(),QString("Server started!"), QSystemTrayIcon::Information, 3 );
        trayIcon->setIcon(QIcon(":/img/jboss_up.png"));
        this->setWindowIcon(QIcon(":/img/jboss_up.png"));
    }
    ui->plainTextEdit->appendPlainText(output);
}

void JbossManager::endMessage()
{
    ui->statusBar->showMessage("Server not running");
    this->setWindowTitle(APP_NAME + " - " + ui->comboBox->currentText());
    ui->plainTextEdit->appendPlainText("##############################\n"
                                       "######  SERVER STOPPED  ######\n"
                                       "##############################\n");
    trayIcon->setToolTip(APP_NAME);
    trayIcon->setIcon(QIcon(":/img/jboss.png"));
    this->setWindowIcon(QIcon(":/img/jboss.png"));
    if (this->isHidden())
        trayIcon->showMessage(ui->comboBox->currentText(),QString("Server stopped!"), QSystemTrayIcon::Information, 3 );
    ui->pushButton->setEnabled(false);
    ui->comboBox->setEnabled(true);
    ui->pushButton_2->setEnabled(true);
    ui->pushButton_2->setText("Start");
    ui->actionStart->setEnabled(true);
    ui->actionStop->setEnabled(false);
    ui->action_Reload->setEnabled(true);
}
void JbossManager::setTitle(QString hub)
{
    this->setWindowTitle(APP_NAME + " - " + hub);
}

void JbossManager::clear()
{
    ui->plainTextEdit->clear();
}

bool JbossManager::quit()
{
    if (!this->isVisible())
    {
        this->raise();
        this->setFocus();
        this->show();
        #if defined (__WIN32__)
        if(IsIconic(this->winId()))
            ShowWindow(this->winId(),SW_RESTORE);
        else
            SetForegroundWindow( this->winId() );
        #endif
    }
    if (!ui->pushButton_2->isEnabled())
    {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, APP_NAME,
                     tr("The server is running.\n"
                        "Kill the server and quit?"),
                     QMessageBox::Yes | QMessageBox::No);
        if (ret == QMessageBox::Yes)
            return true;
        else if (ret == QMessageBox::No)
            return false;
    }
    return true;
}

void JbossManager::search_forward()
{
    QString searchString(ui->lineSearch->text());
    if (searchString!="")
    {
        QTextCursor startPos = ui->plainTextEdit->textCursor();
        QTextCursor cursor = ui->plainTextEdit->document()->find(searchString, startPos);
        if (cursor.isNull())
            cursor = ui->plainTextEdit->document()->find(searchString);
        if (!cursor.isNull()) {
            ui->plainTextEdit->setTextCursor(cursor);
            cursor.setPosition(cursor.selectionEnd());
        }
        ui->plainTextEdit->setFocus();
    }
}

void JbossManager::search_backward()
{
    QString searchString(ui->lineSearch->text());
    if (searchString!="")
    {
        QTextCursor startPos = ui->plainTextEdit->textCursor();
        QTextCursor cursor = ui->plainTextEdit->document()->find(searchString, startPos, QTextDocument::FindBackward);
        if (cursor.isNull())
            cursor = ui->plainTextEdit->document()->find(searchString, QTextDocument::FindBackward);
        if (!cursor.isNull()) {
            ui->plainTextEdit->setTextCursor(cursor);
            cursor.setPosition(cursor.selectionStart());
        }
        ui->plainTextEdit->setFocus();
    }
}

void JbossManager::about_qt()
{
    QMessageBox::aboutQt(this,QString("About Qt"));
}

void JbossManager::about()
{
    QMessageBox::about(this,QString("About"),QString( APP_NAME + " " + APP_VER + "\nAuthor: Albertini Riccardo\n"
                                                      "released under GNU GPL 3"));
}

void JbossManager::openJBFolder()
{
    QString program("file:///");
    program.append(jboss).append("/server/").append(ui->comboBox->currentText());
    QDesktopServices::openUrl(QUrl(program));
}

void JbossManager::settings()
{
    if (!this->isVisible())
        this->show();
    SettingsDialog dialog(this, ui->actionStop->isEnabled());
    if (dialog.exec())
    {
        *jboss = dialog.getJBRoot();
        load();
    }
}

void JbossManager::escEvent()
{
    ui->searchWidget->hide();
    QTextCursor cursor = ui->plainTextEdit->textCursor();
    cursor.movePosition(QTextCursor::End);
}

void JbossManager::getProxy()
{
    QNetworkProxyQuery npq(QUrl("http://www.google.com"));
    QList<QNetworkProxy> listOfProxies = QNetworkProxyFactory::systemProxyForQuery(npq);
    if (listOfProxies.length()>0) {
        QNetworkProxy proxy = (QNetworkProxy)listOfProxies.at(0);
        QNetworkProxy::setApplicationProxy(proxy);
    }
}

void JbossManager::checkUpdate()
{
    QUrl url = QUrl::fromEncoded("http://www.sirowain.com/dev/jboss-manager/VERSION");
    QNetworkRequest request(url);
    reply = manager.get(request);
    connect(reply,SIGNAL(finished()),this,SLOT(readUpdate()));
}

void JbossManager::readUpdate()
{
    if (reply->canReadLine()){
        QFile zipFile("./last-update");
        if (zipFile.open(QIODevice::WriteOnly)) {
            zipFile.write(reply->readAll());
            zipFile.close();

            // Read settings
            QSettings *settings = new QSettings("./last-update", QSettings::IniFormat);
            QString version = settings->value("update/version").toString();
            QString changes = settings->value("update/changes").toString();
            if (APP_VER!=version) {
                QMessageBox msgBox(this);
                msgBox.setText("Update to version " + version + " available!");
                msgBox.setDetailedText(changes);
                msgBox.setInformativeText("Get it from <a \
                                          href=\"https://gitorious.org/jboss-manager/pages/Home\">Gitorious</a>");
                msgBox.setIcon(QMessageBox::Information);
                msgBox.exec();
            }
            zipFile.remove();
        }
    }
}
